package de.aradtke.diagrams_library.circle_diagram;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public enum CircleDiagramAnimationType {
    ALL_AT_ONCE, ONE_AFTER_ANOTHER
}
