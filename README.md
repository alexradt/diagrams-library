# Diagrams Library for Android #
***

## Short Description

This library provides classes for different types of diagrams to show statistics.

***

## CircleDiagram

The CircleDiagram consists of an arbitrary number of Arcs that represent percentage values.
Those Arcs can be animated during the drawing process.

![](./images/circle_diagram_screenshot.png)

####AnimationTypes####

| All at once  | One after another |
| -------------|-------------------|
| ![](./images/circle_diagram_animation_all_at_once.gif) | not implemented yet |

####Usage:####

```java
Arc arc = new Arc(50, "F2514E");
Arc arc1 = new Arc(20, "EFAC19");
Arc arc2 = new Arc(30, "34C1B4");

CircleDiagram circleDiagram = (CircleDiagram) findViewById(R.id.diagram);
// Sets the color of the View Elements Background
circleDiagram.setBackgroundColor(Color.WHITE);
// Sets the color of the circle
circleDiagram.setCircleColor(Color.GRAY);
// Whether to use AntiAliasing or not
circleDiagram.useAntiAliasing(true);
// Sets the stroke width of the circle
circleDiagram.setStrokeWidth(100);
// Sets the margin of the circle to the ViewBounds
circleDiagram.setCircleMargin(20);
// Sets the margin of the arcs to the circle
circleDiagram.setArcMargin(20);
// Adds Arc objects to the Diagram
circleDiagram.addArcs(arc, arc1, arc2);
// Use the Animation or not
// NOTE: Default is true
circleDiagram.useAnimation(true);
// Sets the type of Animation that is used during the draw
circleDiagram.setAnimationType(CircleDiagramAnimationType.ALL_AT_ONCE);
// How long should the Animation be in milliseconds
// NOTE: Default is 1500 ms
circleDiagram.setAnimationDuration(2000);
/* Draws the Diagram
 * NOTE: Must be called if the animation is used.
 * Best way is to use it all the time
 * Can be used to redraw the Diagram after changes
 */
circleDiagram.draw();
/*
 *   Other possibilities to add Arcs
 */
// circleDiagram.addArc(arc);
// circleDiagram.addAllArcs(arcs);
```
and/or

```xml
<de.aradtke.diagrams_library.circle_diagram.CircleDiagram
	android:id=„@+id/diagram“
	android:layout_width=„wrap_content“
	android:layout_height=„wrap_content“
	custom:useAntiAliasing=„true“
	custom:circleColor=„@color/grey“
	custom:strokeWidth=„100“
	custom:circleMargin=„20“
	custom:arcMargin=„20“
	custom:animationDuration=„1500“ />
```

####TODO:####

* Add different Animation styles
* Improve Code
* Add optional legend elements

***

## BarDiagram (Currently not working correctly)

####AnimationTypes####

####Usage####

```java

```

```xml

```

####TODO:####

* Implement BarDiagram
* Think of Animation Types

***

## Gauge (Very basic working version)

####Usage####

```java
Gauge gauge = (Gauge) findViewById(R.id.gauge);
gauge.setNeedleColor(Color.BLUE);
gauge.setUnitArcColor(Color.RED);
gauge.setNeedleWidth(20);
gauge.setGaugeRadius(250);
gauge.setAnimationDuration(2000);
gauge.moveNeedleToAngle(180);
gauge.animateNeedleToAngle(360);
gauge.stopAnimation();
```

```xml
<de.aradtke.diagrams_library.gauge.Gauge
        android:id="@+id/gauge"
        android:layout_height="250dp"
        android:layout_width="250dp" />
```

####TODO:####

* Add segments and texts to the ring
* Provide different Needle types
* Create Sub-Class for different gauge types

***

## Known Issues and Tasks

Have a look at the open Issues

***

## License

```
Copyright 2017 Alexander Radtke

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
