package de.aradtke.diagrams_library.circle_diagram;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class Arc {
    private static final String DEFAULT_COLOR = "#0000FF";

    private Paint mPaint;
    private float mPercentage;
    private float mSweepAngle;
    private float mCurrentSweepAngle = 0.0f;

    public Arc() {
        init();
        setArcPercentage(0.0f);
        setArcColor(DEFAULT_COLOR);
    }

    public Arc(float percentage) {
        init();
        setArcPercentage(percentage);
        setArcColor(DEFAULT_COLOR);
    }

    public Arc(float percentage, String color) {
        init();
        setArcPercentage(percentage);
        setArcColor(color);
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setAntiAlias(true);

        calculateSweepAngle();
    }

    private void calculateSweepAngle() {
        mSweepAngle =  3.6f * mPercentage;
    }

    public void setArcColor(String color) {
        mPaint.setColor(Color.parseColor(color));
    }

    public void setArcColor(int color) {
        mPaint.setColor(color);
    }

    public void setArcPercentage(float percentage) {
        this.mPercentage = percentage;
    }

    protected void setStrokeWidth(int strokeWidth) {
        mPaint.setStrokeWidth(strokeWidth);
    }

    public Paint getPaint() {
        return this.mPaint;
    }

    protected float getSweepAngle() {
        return this.mSweepAngle;
    }

    public float getPercentage() {
        return this.mPercentage;
    }

    protected float getCurrentSweepAngle() {
        return this.mCurrentSweepAngle + 2.0f; // Add 2 to fix the gap between the arcs
    }

    protected void setCurrentSweepAngle(float currentSweepAngle) {
        this.mCurrentSweepAngle = currentSweepAngle;
    }
}
