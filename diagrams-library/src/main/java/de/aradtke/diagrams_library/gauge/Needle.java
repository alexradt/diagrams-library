package de.aradtke.diagrams_library.gauge;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;

/**
 * Represents the needle in the middle of the Gauge, which is drawn at a position
 * relative to the current speed value, etc. <br>
 *
 * The Needle itself consists of four Points, the Center, the left and right base and the
 * Top. <br>
 *
 * <b>This Class should only be used and instantiated by the {@link Gauge} class</b>
 *
 * @author Alexander Radtke
 * @version 1.0
 */
public class Needle {

    private Point needleBase;
    private Point needleLeftBase;
    private Point needleRightBase;
    private Point needleTop;

    private Paint color;

    private int needleWidth;
    private int needleRadius;
    private float angle;

    private int animationDuration;

    public Needle() {
        initPoints();
        this.color = new Paint();
    }

    private void initPoints() {
        this.needleBase = new Point();
        this.needleLeftBase = new Point();
        this.needleRightBase = new Point();
        this.needleTop = new Point();
    }

    private void calculateNeedleLeftBase() {
        this.needleLeftBase.x = (int) (needleWidth * Math.cos(angle - 90)) + this.needleBase.x;
        this.needleLeftBase.y = (int) (needleWidth * Math.sin(angle - 90)) + this.needleBase.y;
    }

    private void calculateNeedleRightBase() {
        this.needleRightBase.x = (int) (needleWidth * Math.cos(angle + 90)) + this.needleBase.x;
        this.needleRightBase.y = (int) (needleWidth * Math.sin(angle + 90)) + this.needleBase.y;
    }

    private void calculateNeedleTop() {
        this.needleTop.x = (int) (needleRadius * Math.cos(angle)) + needleBase.x;
        this.needleTop.y = (int) (needleRadius * Math.sin(angle)) + needleBase.y;
    }

    private Path buildNeedle() {
        calculateNeedleLeftBase();
        calculateNeedleRightBase();
        calculateNeedleTop();

        Path path = new Path();
        path.moveTo(needleBase.x, needleBase.y);
        path.lineTo(needleLeftBase.x, needleLeftBase.y);
        path.lineTo(needleTop.x, needleTop.y);
        path.lineTo(needleRightBase.x, needleRightBase.y);
        path.close();
        return path;
    }

    /**
     * Draws the needle, build by this class, on the given Canvas
     *
     * @param canvas The Canvas on which the needle should be drawn
     */
    public void drawNeedle(Canvas canvas) {
        canvas.drawPath(buildNeedle(), this.color);
    }

    /**
     * Sets the current angle of the Needle. The "current angle" means
     * the position of the Needle on the outer ring of the Gauge, which
     * represents the range of values, e.g. Speed.
     *
     * Max., min. angles are defined by the Gauge of the "outer ring" of
     * it.
     *
     * @param angle The Current angle/position of the Needle
     */
    public void setAngle(float angle) {
        // Convert needleRadius to radian
        this.angle = (float) (angle * Math.PI / 180.0f);
    }

    /**
     * Sets the very center of the Gauge. This Point is used to start
     * drawing the Needle at the exact center of the Gauge
     *
     * @param base The center coordinates of the Gauge
     */
    public void setNeedleBase(Point base) {
        this.needleBase = base;
    }

    /**
     * Sets the Color the Needle should have, when drawn on the canvas
     *
     * @param color The Color of the Needle
     */
    public void setColor(int color) {
        this.color.setColor(color);
    }

    /**
     * Sets the width of the Needle. Actually this is the size of the Needle
     *
     * @param needleWidth The width of the Needle
     */
    public void setNeedleWidth(int needleWidth) {
        this.needleWidth = needleWidth;
    }

    /**
     * Sets the Radius of the Needle. This defines the end position of the Needle on the
     * "ring"
     *
     * @param needleRadius The radius of the Needle
     */
    public void setNeedleRadius(int needleRadius) {
        this.needleRadius = needleRadius;
    }

    /**
     * Sets the animation duration of the needle, when moved to a new angle
     *
     * @param animationDuration The new duration of the animation
     */
    public void setAnimationDuration(int animationDuration) {
        this.animationDuration = animationDuration;
    }

    /**
     * Returns the currently set animation duration
     *
     * @return The current animation duration
     */
    public int getAnimationDuration() {
        return this.animationDuration;
    }
}
