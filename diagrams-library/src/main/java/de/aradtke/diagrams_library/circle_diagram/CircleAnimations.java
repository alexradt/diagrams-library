package de.aradtke.diagrams_library.circle_diagram;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class CircleAnimations {

    /**
     * One of the AnimationTypes.
     * <br>
     * All of the Arcs are animated concurrently, means all start and finish their Animation
     * process at the same time
     * <br>
     * Correspondents with {@link CircleDiagramAnimationType}
     *
     * @param view
     * @param arcs
     * @param animDuration
     */
    protected static void animateArcsAllAtOnce(final View view, final ArrayList<Arc> arcs,
                                               int animDuration) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(getArcAnimators(view, arcs, animDuration));
        set.start();
    }

    /**
     * One of the AnimationTypes.
     * <br>
     * All of the Arcs are animated one after the other. Once a Arc finished his Animation
     * the next one starts and so on
     * <br>
     * Correspondents with {@link CircleDiagramAnimationType}
     *
     * @param view
     * @param arcs
     * @param animDuration
     */
    protected static void animateArcsOneAfterAnother(final View view, final ArrayList<Arc> arcs,
                                                     int animDuration) {
        AnimatorSet set = new AnimatorSet();
        set.playSequentially(getArcAnimators(view, arcs, animDuration));
        set.start();
    }

    /**
     *
     *
     * @param view
     * @param arcs
     * @param animDuration
     * @return
     */
    private static ArrayList<Animator> getArcAnimators(final View view, final ArrayList<Arc> arcs,
                                                       int animDuration) {
        ArrayList<Animator> animators = new ArrayList<>();
        for (int i = 0; i < arcs.size(); i++) {
            // FIXME: bad code
            final int inner_i = i;

            ValueAnimator animator = ValueAnimator.ofFloat(0, arcs.get(i).getSweepAngle());
            animator.setDuration(TimeUnit.MILLISECONDS.toMillis(animDuration));
            animator.setInterpolator(new LinearInterpolator());
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    arcs.get(inner_i).setCurrentSweepAngle((float) animation.getAnimatedValue());
                    view.invalidate();
                }
            });
            animators.add(animator);
        }
        return animators;
    }
}
