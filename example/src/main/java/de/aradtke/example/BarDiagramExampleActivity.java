package de.aradtke.example;

import android.app.Activity;
import android.os.Bundle;

import de.aradtke.diagrams_library.bar_diagram.Bar;
import de.aradtke.diagrams_library.bar_diagram.BarDiagram;
import de.aradtke.diagrams_library.bar_diagram.HorizontalBarDiagram;
import de.aradtke.diagrams_library.bar_diagram.VerticalBarDiagram;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class BarDiagramExampleActivity extends Activity {
    private BarDiagram mBarDiagram;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bar_diagram_example_layout);

        Bar bar1 = new Bar("#F2514E", 60);
        Bar bar2 = new Bar("#EFAC19", 40);
        Bar bar3 = new Bar("#F2514E", 80);
        Bar bar4 = new Bar("#EFAC19", 50);

        mBarDiagram = (HorizontalBarDiagram) findViewById(R.id.horizontal_bar_diagram);
        // mBarDiagram = (VerticalBarDiagram) findViewById(R.id.vertical_bar_diagram);

        mBarDiagram.setBackgroundColor("#34C1B4");
        mBarDiagram.setDiagramMargin(20);
        mBarDiagram.addBar(bar1);
        mBarDiagram.addBar(bar2);
        mBarDiagram.addBar(bar3);
        mBarDiagram.addBar(bar4);
    }
}
