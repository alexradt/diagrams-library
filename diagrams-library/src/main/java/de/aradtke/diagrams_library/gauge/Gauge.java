package de.aradtke.diagrams_library.gauge;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import java.util.concurrent.TimeUnit;

/**
 * Gauge base class. Represents a basic Gauge, with a Center, a Needle and a Ring,
 * which represents the Unit and the current value
 *
 * @author Alexander Radtke
 * @version 1.0
 */
public class Gauge extends View {

    private Needle needle;
    private UnitArc unitArc;
    private Configuration configuration;
    private ValueAnimator animator;

    private float lastAngle;

    public Gauge(Context context) {
        super(context);
        init();
    }

    public Gauge(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        configuration = new Configuration(context, attrs);
        init();
    }

    public Gauge(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        configuration = new Configuration(context, attrs);
        init();
    }

    public Gauge(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        configuration = new Configuration(context, attrs);
        init();
    }

    private void init() {
        initNeedle();
        initUnitArc();
    }

    private void initNeedle() {
        this.needle = new Needle();
        this.needle.setNeedleRadius(configuration.getGaugeRadius());
        this.needle.setNeedleWidth(configuration.getNeedleWidth());
        this.needle.setColor(configuration.getNeedleColor());
        this.needle.setAnimationDuration(configuration.getAnimationDuration());
    }

    private void initUnitArc() {
        this.unitArc = new UnitArc();
        this.unitArc.setUnitArcRadius(configuration.getGaugeRadius());
        this.unitArc.setColor(configuration.getUnitArcColor());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawGaugeCenter(canvas);
        this.unitArc.drawUnitArc(canvas);
        this.needle.drawNeedle(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        this.needle.setNeedleBase(getCenterOfView());
        this.unitArc.setUnitArcCenter(getCenterOfView());

        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    private Point getCenterOfView() {
        Point point = new Point();

        point.set(this.getMeasuredWidth() / 2,
                this.getMeasuredHeight() / 2);

        return point;
    }

    private void drawGaugeCenter(Canvas canvas) {
        Point center = getCenterOfView();
        Paint paint = new Paint();
        paint.setColor(Color.GRAY);
        canvas.drawCircle(center.x, center.y, 50, paint);
    }

    /**
     * Moves the Needle to a certain position on the "ring" of the Gauge, by animating it.
     * Since the range of the Gauge is 0 to 180 degrees the passed angle should
     * be somewhere between those values. <br>
     *
     * <b>The default animation duration is {@link ConfigurationDefault#DEFAULT_ANIMATION_DURATION}</b>
     * The duration of the animation can be changed by using {@link #setAnimationDuration(int)}
     *
     * @param angle The angle to which the Needle should be moved (0 - 180)
     */
    public void animateNeedleToAngle(int angle) {

        animator = ValueAnimator.ofFloat(lastAngle, angle);
        animator.setDuration(TimeUnit.MILLISECONDS.toMillis(needle.getAnimationDuration()));
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Gauge.this.lastAngle = (float) animation.getAnimatedValue();
                Gauge.this.needle.setAngle((float) animation.getAnimatedValue());
                Gauge.this.invalidate();
            }
        });
        animator.start();
    }

    /**
     * Moves the Needle to a certain position on the "ring" of the Gauge, without animating it.
     * Since the range of the Gauge is 0 to 180 degrees the passed angle should
     * be somewhere between those values. <br>
     *
     * <b>The default animation duration is {@link ConfigurationDefault#DEFAULT_ANIMATION_DURATION}</b>
     * The duration of the animation can be changed by using {@link #setAnimationDuration(int)}
     *
     * @param angle The angle to which the Needle should be moved (0 - 180)
     */
    public void moveNeedleToAngle(int angle) {
        this.lastAngle = angle;
        this.needle.setAngle(angle);
        this.invalidate();
    }

    /**
     * Stops the current animation process of the Needle. The current
     * position of the Needle, at which the animations is stopped,
     * is kept for further changes
     */
    public void stopAnimation() {
        animator.cancel();
    }

    /**
     * Sets the Color of the Needle. All colors of {@link Color} class
     * are allowed for this method. <br>
     *
     * <b>The default color of the Needle is {@link Color#BLACK}</b>
     *
     * @param color An Enum of {@link Color}
     */
    public void setNeedleColor(int color) {
        this.needle.setColor(color);
    }

    /**
     * Sets the width or size of the Needle itself. <br>
     *
     * <b>The default value for the width of the Needle is {@link ConfigurationDefault#DEFAULT_NEEDLE_WIDTH}</b>
     *
     * @param needleWidth The width/size of the Needle
     */
    public void setNeedleWidth(int needleWidth) {
        this.needle.setNeedleWidth(needleWidth);
    }

    /**
     * Sets the Color of the Arc on which the unit is presented. All colors of {@link Color} class
     * are allowed for this method. <br>
     *
     * <b>The default color of the Needle is {@link Color#BLACK}</b>
     *
     * @param color An Enum of {@link Color}
     */
    public void setUnitArcColor(int color) {
        this.unitArc.setColor(color);
    }

    /**
     * Sets the radius/width of the complete Gauge. This will change the radius of the Needle,
     * as well as the radius of the "ring". Both need to be drawn equally in terms of the
     * radius in order to look nice. <br>
     *
     * <b>The default radius for the radius of the Gauge is {@link ConfigurationDefault#DEFAULT_GAUGE_RADIUS}</b>
     *
     * @param gaugeRadius The radius of the Gauge
     */
    public void setGaugeRadius(int gaugeRadius) {
        this.needle.setNeedleRadius(gaugeRadius);
        this.unitArc.setUnitArcRadius(gaugeRadius);
    }

    /**
     * Sets the duration of the Animation, that is performed by the Needle, when {@link #animateNeedleToAngle(int)}
     * is called. <br>
     *
     * <b>The default animation duration is {@link ConfigurationDefault#DEFAULT_ANIMATION_DURATION}</b>
     * The duration of the animation can be changed by using {@link #setAnimationDuration(int)}
     *
     * @param animationDuration The new duration of the Animation
     */
    public void setAnimationDuration(int animationDuration) {
        this.needle.setAnimationDuration(animationDuration);
    }
}
