package de.aradtke.diagrams_library.circle_diagram;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

import de.aradtke.diagrams_library.R;
import de.aradtke.diagrams_library.exception.InvalidPercentageSumException;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class CircleDiagram extends View {
    private static final float VALID_CIRCLE_SUM = 100.0f;

    private Paint mBackgroundPaint;
    private Paint mCirclePaint;

    private boolean mUseAnimation;
    private int mStrokeWidth;
    private int mCircleMargin;
    private int mArcMargin;
    private int[] mOval;
    private int mAnimationDuration;

    private CircleDiagramAnimationType mAnimationType;

    private ArrayList<Arc> arcs;

    /**
     * @see View
     *
     * @param context
     */
    public CircleDiagram(Context context) {
        super(context);
        init();
    }

    /**
     * @see View
     *
     * @param context
     * @param attributeSet
     */
    public CircleDiagram(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
        readAttributes(context, attributeSet);
    }

    /**
     * @see View
     *
     * @param context
     * @param attributeSet
     * @param defStyleAttr
     */
    public CircleDiagram(Context context, AttributeSet attributeSet, int defStyleAttr) {
        super(context, attributeSet, defStyleAttr);
        init();
        readAttributes(context, attributeSet);
    }

    private void init() {
        arcs = new ArrayList<>();

        mBackgroundPaint = new Paint();
        mCirclePaint = new Paint();

        // Use Animation by default
        mUseAnimation = true;
        // Set default values to prevent errors
        mStrokeWidth = getResources().getInteger(R.integer.default_circle_stroke_width);
        mCircleMargin = getResources().getInteger(R.integer.default_circle_margin);
        mArcMargin = getResources().getInteger(R.integer.default_arc_margin);
        // Duration of the Animation in milliseconds
        mAnimationDuration = R.integer.default_animation_duration;
        mAnimationType = null;
        mOval = new int[4];
    }

    /**
     * Reads the Attributes from the layout file. Will be overridden if values
     * are changed in Java code
     *
     * @param context
     * @param attrs
     */
    private void readAttributes(Context context, AttributeSet attrs) {
        TypedArray data = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CircleDiagram, 0, 0);

        try {
            boolean useAntiAliasing = data.getBoolean(R.styleable.CircleDiagram_useAntiAliasing, true);
            useAntiAliasing(useAntiAliasing);
            mCirclePaint.setColor(data.getColor(R.styleable.CircleDiagram_circleColor, Color.GRAY));
            mStrokeWidth = data.getInteger(R.styleable.CircleDiagram_strokeWidth,
                    R.integer.default_circle_stroke_width);
            mCircleMargin = data.getInteger(R.styleable.CircleDiagram_circleMargin,
                    R.integer.default_circle_margin);
            mArcMargin = data.getInteger(R.styleable.CircleDiagram_arcMargin,
                    R.integer.default_arc_margin);
            mAnimationDuration = data.getInteger(R.styleable.CircleDiagram_animationDuration,
                    R.integer.default_animation_duration);
        } finally {
            data.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw Circle and all Arcs onto the Canvas
        canvas.drawPaint(mBackgroundPaint);
        drawCircle(canvas);
        drawArcs(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // Makes the View Square, which solves the layout Issue for now
        int size;
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        if (width > height) {
            size = height;
        } else {
            size = width;
        }

        setMeasuredDimension(size, size);
    }

    /**
     * Draws the previously calculated Circle onto the Canvas
     *
     * @param canvas Canvas object from onDraw()
     */
    private void drawCircle(Canvas canvas) {
        int[] center = getCenterOfView();
        int radius = calculateRadius(center);
        calculateOval(radius);
        mCirclePaint.setStrokeWidth(mStrokeWidth);
        mCirclePaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(center[0], center[1], radius, mCirclePaint);
    }

    /**
     * Draws all Arcs onto the Canvas and inside the Circle. Also does some checks.
     *
     * @param canvas Canvas object from onDraw()
     */
    private void drawArcs(Canvas canvas) {
        if (!arcs.isEmpty()) {
            if (calculateCircleSum()) {
                if (mUseAnimation) {
                    drawArcsWithAnimation(canvas);
                } else {
                    drawArcsWithoutAnimation(canvas);
                }
            } else {
                throw new InvalidPercentageSumException(getResources().getString(R.string.invalid_circle_sum_message));
            }
        }
    }

    /**
     *
     * @param canvas
     */
    private void drawArcsWithoutAnimation(Canvas canvas) {
        int sum = 0;

        for (int i = 0; i < arcs.size(); i++) {
            arcs.get(i).setStrokeWidth(mStrokeWidth - mArcMargin);

            canvas.drawArc(mOval[0], mOval[1], mOval[2], mOval[3],
                    // FIXME: Adding 2 to the sweepAngle should be done somewhere else
                    (i == 0) ? 0 : sum, arcs.get(i).getSweepAngle() + 2.0f, false, arcs.get(i).getPaint());
            sum += arcs.get(i).getSweepAngle();
        }
    }

    /**
     *
     * @param canvas
     */
    private void drawArcsWithAnimation(Canvas canvas) {
        int sum = 0;

        for (int i = 0; i < arcs.size(); i++) {
            arcs.get(i).setStrokeWidth(mStrokeWidth - mArcMargin);

            canvas.drawArc(mOval[0], mOval[1], mOval[2], mOval[3],
                    (i == 0) ? 0 : sum, arcs.get(i).getCurrentSweepAngle(), false, arcs.get(i).getPaint());
            sum += arcs.get(i).getSweepAngle();
        }
    }

    /**
     * Calculates the Oval on which the Arcs are placed later on
     * To Calculate this the radius of the Circle and the center of this View are used
     *
     * @param radius int[0] = left | int[1] = top | int[2] = right | int[3] = bottom |
     */
    private void calculateOval(int radius) {
        int[] centers = getCenterOfView();

        int left = (centers[0] - radius);
        int top = (centers[1] - radius);
        int right = (centers[0] + radius);
        int bottom = (centers[1] + radius);

        mOval = new int[]{left, top, right, bottom};
    }

    /**
     * Calculates the center of this View object
     *
     * @return int[0] = width | int[1] = height
     */
    private int[] getCenterOfView() {
        int width = this.getWidth() / 2;
        int height = this.getHeight() / 2;

        return new int[]{width, height};
    }

    /**
     * Since the Circle should fill it's View the Radius of the Circle must be calculated.
     * The size depends on the width or height of the View
     *
     * @param center The Center of the View which is calculated through getCenterOfView()
     * @return The biggest radius that is possible on the View
     */
    private int calculateRadius(int[] center) {
        final int radius;

        if (center[0] > center[1]) {
            radius = center[1] - (mCircleMargin + mStrokeWidth);
        } else if (center[0] < center[1]) {
            radius = center[0] - (mCircleMargin + mStrokeWidth);
        } else if (center[0] == center[1]) {
            radius = center[0] - (mCircleMargin + mStrokeWidth);
        } else {
            radius = 0;
        }
        return radius;
    }

    /**
     * Checks whether the sum of all Arcs is 100. Otherwise the Circle is not fully filled
     * or more than full
     *
     * @return true == sum is valid | false == sum is invalid
     */
    private boolean calculateCircleSum() {
        float sum = 0;
        for (Arc arc : arcs) {
            sum += arc.getPercentage();
        }

        if (sum == VALID_CIRCLE_SUM) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks which Animation was set and calls the correct method
     */
    private void animateArcs() {
        if (mAnimationType == CircleDiagramAnimationType.ALL_AT_ONCE) {
            CircleAnimations.animateArcsAllAtOnce(this, arcs, mAnimationDuration);
        } else if (mAnimationType == CircleDiagramAnimationType.ONE_AFTER_ANOTHER) {
            CircleAnimations.animateArcsOneAfterAnother(this, arcs, mAnimationDuration);
        } else {
            // When no animation type was set use ALL_AT_ONCE
            CircleAnimations.animateArcsAllAtOnce(this, arcs, mAnimationDuration);
        }
    }

    /**
     * Adds a single Arc to the Diagram surface
     *
     * @param arc
     */
    public void addArc(Arc arc) {
        arcs.add(arc);
    }

    /**
     * Adds a arbitrary number of Arcs to the Diagram surface
     *
     * @param arcs
     */
    public void addArcs(Arc... arcs) {
        for (Arc arc : arcs) {
            this.arcs.add(arc);
        }
    }

    /**
     * Adds Arcs to the Diagram surface through an ArrayList
     *
     * @param arcs
     */
    public void addAllArcs(ArrayList<Arc> arcs) {
        this.arcs = arcs;
    }

    /**
     * Removes a single Arc object from the Diagram surface
     *
     * @param arc
     */
    public void removeArc(Arc arc) {
        arcs.remove(arc);
    }

    /**
     * Sets the AnimationType which will be used when drawing the Arcs on the Canvas
     *
     * @param type One of the pre-defined AnimationTypes
     */
    public void setAnimationType(CircleDiagramAnimationType type) {
        this.mAnimationType = type;
    }

    /**
     * Overrides the default setBackground method and sets the BackgroundColor of a specific Paint
     * object instead
     *
     * @param color
     */
    @Override
    public void setBackgroundColor(int color) {
        mBackgroundPaint.setColor(color);
    }

    /**
     * Sets the color of the circle. Uses an resource or {@link Color} value
     *
     * @param color
     */
    public void setCircleColor(int color) {
        mCirclePaint.setColor(color);
    }

    /**
     * Transforms a hex color String to a Color which is used to colorize the circle
     *
     * @param color The String which contains the color in hex
     */
    public void setCircleColor(String color) {
        mCirclePaint.setColor(Color.parseColor(color));
    }

    /**
     * Sets the StrokeWidth of the circle.
     * <br>
     * This value should be over 100 if the whole Screen should be filled with the Diagram
     *
     * @param strokeWidth The default value for the StrokeWidth is 80
     */
    public void setStrokeWidth(int strokeWidth) {
        this.mStrokeWidth = strokeWidth;
    }

    /**
     * Sets the margin between the circle and the bounds of the View
     *
     * @param margin The default value for the CircleMargin is 10
     */
    public void setCircleMargin(int margin) {
        this.mCircleMargin = margin;
    }

    /**
     * Sets the margin between the circle and the Arcs that are inside the circle
     * <br>
     * A value of 0 would mean that there is no margin so the circle wouldn't be visible at all
     *
     * @param margin The default value for the ArcMargin is 20
     */
    public void setArcMargin(int margin) {
        this.mArcMargin = margin;
    }

    /**
     * Sets the duration of the arc Animation in milliseconds
     *
     * @param duration The duration in milliseconds. The default value is 1500 ms
     */
    public void setAnimationDuration(int duration) {
        this.mAnimationDuration = duration;
    }

    /**
     * AntiAliasing provides a smoother drawing of the Elements without any sharp edges
     *
     * @param useAntiAliasing true == use AntiAliasing | false == don't use AntiAliasing
     */
    public void useAntiAliasing(boolean useAntiAliasing) {
        mBackgroundPaint.setAntiAlias(useAntiAliasing);
        mCirclePaint.setAntiAlias(useAntiAliasing);
    }

    /**
     * Defines whether to use an Animation for the Arcs or not
     * <br>
     * On default the Animation is set to true (active)
     *
     * @param useAnimation true == use Animation | false == don't use Animation
     */
    public void useAnimation(boolean useAnimation) {
        this.mUseAnimation = useAnimation;
    }

    /**
     * Draws the Canvas. Can be used to update the Diagram
     */
    public void draw() {
        if(mUseAnimation) {
            animateArcs();
        } else {
            this.invalidate();
        }
    }
}
