package de.aradtke.diagrams_library.gauge;

import android.graphics.Color;

/**
 * Contains all configuration default values, which are used in case that no value is definied
 * in the layout xml definition.
 *
 * @author Alexander Radtke
 * @version 1.0
 */
public interface ConfigurationDefault {

    int DEFAULT_NEEDLE_COLOR = Color.BLACK;
    int DEFAULT_NEEDLE_WIDTH = 20;
    int DEFAULT_UNIT_ARC_COLOR = Color.BLACK;
    int DEFAULT_GAUGE_RADIUS = 250;
    int DEFAULT_ANIMATION_DURATION = 1500;

}
