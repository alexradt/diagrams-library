package de.aradtke.diagrams_library.exception;

/**
 * @author Alexander Radtke
 */
public class InvalidPercentageSumException extends RuntimeException {

    public InvalidPercentageSumException() {
        super();
    }

    public InvalidPercentageSumException(String message) {
        super(message);
    }
}
