package de.aradtke.diagrams_library.bar_diagram;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;

import de.aradtke.diagrams_library.R;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public abstract class BarDiagram extends View {

    private Paint mBackgroundPaint;

    protected int mDiagramMargin;
    protected int mBarMargin;
    protected int mBarHeight;
    protected int mBarWidth;

    protected int mViewWidth;
    protected int mViewHeight;

    protected ArrayList<Bar> mBars;

    /**
     * @see View
     *
     * @param context
     */
    public BarDiagram(Context context) {
        super(context);
        init();
    }

    /**
     * @see View
     *
     * @param context
     * @param attributeSet
     */
    public BarDiagram(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    /**
     * @see View
     *
     * @param context
     * @param attributeSet
     * @param defStyleAttr
     */
    public BarDiagram(Context context, AttributeSet attributeSet, int defStyleAttr) {
        super(context, attributeSet, defStyleAttr);
        init();
    }

    private void init() {
        mBackgroundPaint = new Paint();

        mDiagramMargin = getResources().getInteger(R.integer.default_bar_diagram_margin);
        mBarMargin = 20;

        mBars = new ArrayList<>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawPaint(mBackgroundPaint);
        drawRect(canvas);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        // Get the Width and the Height of the View
        this.mViewWidth = this.getMeasuredWidth();
        this.mViewHeight = this.getMeasuredHeight();

        measureRectSize();
        setUpRect();
        this.invalidate();
    }

    abstract void drawRect(Canvas canvas);
    abstract void measureRectSize();
    abstract void setUpRect();

    /**
     *
     * @param diagramMargin
     */
    public void setDiagramMargin(int diagramMargin) {
        this.mDiagramMargin = diagramMargin;
    }

    /**
     *
     * @param bar
     */
    public void addBar(Bar bar) {
        this.mBars.add(bar);
    }

    /**
     *
     * @param bar
     */
    public void removeBar(Bar bar) {
        this.mBars.remove(bar);
        measureRectSize();
        setUpRect();
    }

    /**
     *
     * @param color
     */
    public void setBackgroundColor(String color) {
        this.mBackgroundPaint.setColor(Color.parseColor(color));
    }
}
