package de.aradtke.diagrams_library.bar_diagram;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class VerticalBarDiagram extends BarDiagram {

    /**
     * @see BarDiagram
     *
     * @param context
     */
    public VerticalBarDiagram(Context context) {
        super(context);
    }

    /**
     * @see BarDiagram
     *
     * @param context
     * @param attributeSet
     */
    public VerticalBarDiagram(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /**
     * @see BarDiagram
     *
     * @param context
     * @param attributeSet
     * @param defStyleAttr
     */
    public VerticalBarDiagram(Context context, AttributeSet attributeSet, int defStyleAttr) {
        super(context, attributeSet, defStyleAttr);
    }

    @Override
    void drawRect(Canvas canvas) {

    }

    @Override
    void measureRectSize() {

    }

    @Override
    void setUpRect() {

    }
}
