package de.aradtke.example;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import de.aradtke.diagrams_library.gauge.Gauge;

/**
 * @author Alexander Radtke
 * @version 1.0
 */
public class GaugeExampleActivity extends Activity implements View.OnClickListener {

    private Gauge gauge;
    private Button animateButton;

    private boolean isFirstPress = true;

    @Override
    public void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        setContentView(R.layout.gauge_example_layout);

        animateButton = (Button) findViewById(R.id.animate);
        animateButton.setOnClickListener(this);

        gauge = (Gauge) findViewById(R.id.gauge);
        gauge.setNeedleColor(Color.BLUE);
        gauge.moveNeedleToAngle(180);
    }

    @Override
    public void onClick(View v) {
        if (isFirstPress) {
            gauge.animateNeedleToAngle(360);
            isFirstPress = false;
        } else {
            gauge.stopAnimation();
            gauge.animateNeedleToAngle(180);
            isFirstPress = true;
        }
    }
}
