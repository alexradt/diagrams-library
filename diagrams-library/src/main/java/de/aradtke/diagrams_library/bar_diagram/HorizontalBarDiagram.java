package de.aradtke.diagrams_library.bar_diagram;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class HorizontalBarDiagram extends BarDiagram {

    /**
     * @see BarDiagram
     *
     * @param context
     */
    public HorizontalBarDiagram(Context context) {
        super(context);
    }

    /**
     * @see BarDiagram
     *
     * @param context
     * @param attributeSet
     */
    public HorizontalBarDiagram(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /**
     * @see BarDiagram
     *
     * @param context
     * @param attributeSet
     * @param defStyleAttr
     */
    public HorizontalBarDiagram(Context context, AttributeSet attributeSet, int defStyleAttr) {
        super(context, attributeSet, defStyleAttr);
    }

    @Override
    void drawRect(Canvas canvas) {
        for (Bar bar : mBars) {
            canvas.drawRect(bar.getRect(), bar.getBarPaint());
        }
    }

    @Override
    void measureRectSize() {
        mBarWidth = mViewWidth - mDiagramMargin;
        Log.d("Measured Bar Width", Integer.toString(mBarWidth));
        // the height of every single Bar depends on how many Bars are used
        // therefore the calculation is a bit more complicated
        mBarHeight =
                (mViewHeight - (mBarMargin * mBars.size())) / mBars.size();
        Log.d("Measured Bar Height", Integer.toString(mBarHeight));
    }

    @Override
    void setUpRect() {
        int yPosCounter = 0;

        for (int i = 0; i < mBars.size(); i++) {
            Rect r = mBars.get(i).getRect();

            int topLeftX = mDiagramMargin;
            int topLeftY = (i == 0) ? mDiagramMargin : (yPosCounter);
            yPosCounter += (mBarMargin + mBarHeight);

            int bottomRightX = mBarWidth;
            int bottomRightY = (i == 0) ? mBarHeight : ((i + 1) * mBarHeight) + mBarMargin;
            Log.d("bottomRightY", Integer.toString(((i + 1) * mBarHeight)));

            Log.d("setUpRect", "Top Coordinates: " + topLeftX + " " + topLeftY +
            " " + bottomRightX + " " + bottomRightY);

            r.set(topLeftX, topLeftY, bottomRightX, bottomRightY);
        }
    }
}
