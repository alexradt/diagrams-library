package de.aradtke.diagrams_library.gauge;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

/**
 * Represents the "ring", on which the current value is displayed. Also defines the unit, by e.g.
 * drawing labels at certain positions
 *
 * @author Alexander Radtke
 * @version 1.0
 */
public class UnitArc {

    private Point center;
    private int radius;
    private int startAngle;
    private int sweepAngle;

    private Paint paint;

    private int[] oval;

    public UnitArc() {
        this.center = new Point(0,0);

        this.startAngle = 20;
        this.sweepAngle = -220;

        this.paint = new Paint();
        this.paint.setStrokeWidth(100);
        this.paint.setStyle(Paint.Style.STROKE);
    }

    /**
     * Draws the "ring" on the given canvas, with calculated oval from the radius
     *
     * @param canvas The canvas to draw on
     */
    public void drawUnitArc(Canvas canvas) {
        canvas.drawArc(oval[0], oval[1], oval[2], oval[3], startAngle, sweepAngle, false, paint);
    }

    /**
     * Sets the center position of the Gauge, this is used to calculate the oval for drawing the "ring"
     *
     * @param center The center position of the Gauge
     */
    public void setUnitArcCenter(Point center) {
        this.center = center;
        calculateOval();
    }

    /**
     * Sets the color of the "ring"
     *
     * @param color The color of the "ring"
     */
    public void setColor(int color) {
        this.paint.setColor(color);
    }

    /**
     * Sets the stroke width of the ring
     *
     * @param strokeWidth The stroke width
     */
    public void setStrokeWidth(int strokeWidth) {
        this.paint.setStrokeWidth(strokeWidth);
    }

    /**
     * Sets the start angle. The arc is drawn clockwise.
     * An angle of 0 degrees correspond to the geometric angle of 0 degrees (3 o'clock on a watch.)
     *
     * @param startAngle The start angle
     */
    public void setStartAngle(int startAngle) {
        this.startAngle = startAngle;
    }

    /**
     * Sets the sweep angle.
     * To draw clockwise us positive values <= 360.
     * To draw counterclockwise use negative values <= -360
     *
     * @param sweepAngle The sweep angle
     */
    public void setSweepAngle(int sweepAngle) {
        this.sweepAngle = sweepAngle;
    }

    /**
     * Sets the radius, which is used for calculation
     *
     * @param radius The radius
     */
    public void setUnitArcRadius(int radius) {
        this.radius = radius;
    }

    /**
     * Calculates the Oval on which the Arc is placed later on
     * To Calculate this the radius of the Circle and the center of the View are used
     */
    private void calculateOval() {
        int left = (center.x - radius);
        int top = (center.y - radius);
        int right = (center.x + radius);
        int bottom = (center.y + radius);

        oval = new int[]{left, top, right, bottom};
    }
}
