package de.aradtke.example;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import de.aradtke.diagrams_library.circle_diagram.Arc;
import de.aradtke.diagrams_library.circle_diagram.CircleDiagram;
import de.aradtke.diagrams_library.circle_diagram.CircleDiagramAnimationType;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class CircleDiagramExampleActivity extends Activity implements View.OnClickListener {
    private CircleDiagram circleDiagram;
    private Button button;

    @Override
    public void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        setContentView(R.layout.circle_diagram_example_layout);

        button = (Button) findViewById(R.id.animate);
        button.setOnClickListener(this);

        /*
         *   Create two Arcs with a percentage value and a specific color
         *
         *   NOTE: The sum of the Arc percentage values must be 100, otherwise
         *         an InvalidPercentageSumException will be thrown
         */
        Arc arc = new Arc(50, "#F2514E");
        Arc arc1 = new Arc(20, "#EFAC19");
        Arc arc2 = new Arc(30, "#34C1B4");

        circleDiagram = (CircleDiagram) findViewById(R.id.circle_diagram);
        // Sets the color of the View Elements Background
        circleDiagram.setBackgroundColor(Color.WHITE);
        // Sets the color of the circle
        circleDiagram.setCircleColor(Color.GRAY);
        // Whether to use AntiAliasing or not
        circleDiagram.useAntiAliasing(true);
        // Sets the stroke width of the circle
        circleDiagram.setStrokeWidth(100);
        // Sets the margin of the circle to the ViewBounds
        circleDiagram.setCircleMargin(20);
        // Sets the margin of the arcs to the circle
        circleDiagram.setArcMargin(20);
        // Adds Arc objects to the Diagram
        circleDiagram.addArcs(arc, arc1, arc2);
        // Use the Animation or not
        // NOTE: Default is true
        circleDiagram.useAnimation(true);
        // Sets the type of Animation that is used during the draw
        circleDiagram.setAnimationType(CircleDiagramAnimationType.ONE_AFTER_ANOTHER);
        // How long should the Animation be in milliseconds
        // NOTE: Default is 1500 ms
        circleDiagram.setAnimationDuration(1000);
        /* Draws the Diagram
         * NOTE: Must be called if the animation is used.
         * Best way is to use it all the time
         * Can be used to redraw the Diagram after changes
         */
        circleDiagram.draw();
        /*
         *   Other possibilities to add Arcs
         */
        // circleDiagram.addArc(arc);
        // circleDiagram.addAllArcs(arcs);
    }

    @Override
    public void onClick(View view) {
        circleDiagram.draw();
    }
}
