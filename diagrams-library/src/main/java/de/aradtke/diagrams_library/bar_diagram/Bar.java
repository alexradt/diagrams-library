package de.aradtke.diagrams_library.bar_diagram;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import de.aradtke.diagrams_library.R;
import de.aradtke.diagrams_library.exception.InvalidPercentageValueException;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class Bar {
    private static final String INVALID_PERCENTAGE_VALUE_HIGH =
            "The percentage value must be smaller or equal 100";
    private static final String INVALID_PERCENTAGE_VALUE_LOW =
            "The percentage value must be bigger or equal 0";

    private Paint mBarPaint;
    private Rect mRect;
    private float mPercentage;

    public Bar() {
        init();
    }

    public Bar(String color) {
        init();
        setBarColor(color);
    }

    public Bar(String color, float percentage) {
        init();
        setBarColor(color);
        setPercentage(percentage);
    }

    private void init() {
        mBarPaint = new Paint();
        mBarPaint.setStyle(Paint.Style.FILL);
        mBarPaint.setAntiAlias(true);
        mRect = new Rect();
    }

    private void checkPercentageValue() {
        if (mPercentage > 100.0) {
            throw new InvalidPercentageValueException(INVALID_PERCENTAGE_VALUE_HIGH);
        } else if (mPercentage < 0.0) {
            throw new InvalidPercentageValueException(INVALID_PERCENTAGE_VALUE_LOW);
        }
    }

    public float getPercentage() {
        return mPercentage;
    }

    public void setPercentage(float percentage) {
        this.mPercentage = percentage;
        checkPercentageValue();
    }

    public void setBarColor(String color) {
        this.mBarPaint.setColor(Color.parseColor(color));
    }

    protected Paint getBarPaint() {
        return this.mBarPaint;
    }

    protected Rect getRect() {
        return this.mRect;
    }
}
