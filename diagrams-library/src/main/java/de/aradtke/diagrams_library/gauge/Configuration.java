package de.aradtke.diagrams_library.gauge;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import de.aradtke.diagrams_library.R;

/**
 * Reads style attributes from the layout xml definition of the Gauge. The Configuration
 * can be accessed by using getters. A value is never null, there will always be a default
 * value available from {@link ConfigurationDefault}
 *
 * @author Alexander Radtke
 * @version 1.0
 */
public class Configuration implements ConfigurationDefault {

    private int needleColor;
    private int needleWidth;
    private int unitArcColor;
    private int gaugeRadius;
    private int animationDuration;

    public Configuration(Context context, AttributeSet attributeSet) {
        TypedArray data = context.getTheme().obtainStyledAttributes(attributeSet,
                R.styleable.Gauge, 0, 0);

        readAttributes(data);
    }

    private void readAttributes(TypedArray attributes) {
        this.needleColor = attributes.getInteger(R.styleable.Gauge_needleColor, DEFAULT_NEEDLE_COLOR);
        this.needleWidth = attributes.getInteger(R.styleable.Gauge_needleWidth, DEFAULT_NEEDLE_WIDTH);
        this.unitArcColor = attributes.getInteger(R.styleable.Gauge_unitArcColor, DEFAULT_UNIT_ARC_COLOR);
        this.gaugeRadius = attributes.getInteger(R.styleable.Gauge_gaugeRadius, DEFAULT_GAUGE_RADIUS);
        this.animationDuration = attributes.getInteger(R.styleable.Gauge_needleAnimationDuration, DEFAULT_ANIMATION_DURATION);
    }

    public int getNeedleColor() {
        return this.needleColor;
    }

    public int getNeedleWidth() {
        return this.needleWidth;
    }

    public int getUnitArcColor() {
        return this.unitArcColor;
    }

    public int getGaugeRadius() {
        return this.gaugeRadius;
    }

    public int getAnimationDuration() {
        return this.animationDuration;
    }
}
