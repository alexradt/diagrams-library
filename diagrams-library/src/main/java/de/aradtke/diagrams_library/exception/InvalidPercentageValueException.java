package de.aradtke.diagrams_library.exception;

/**
 * @author Alexander Radtke
 * @version 0.1
 */
public class InvalidPercentageValueException extends RuntimeException {

    public InvalidPercentageValueException() {
        super();
    }

    public InvalidPercentageValueException(String message) {
        super(message);
    }
}
